<?php
/**
 * Created by argonavt.
 * Date: 13/07/17
 * Time: 10:06 AM
 */

namespace Modules\Blog\Models;
use \System\Model;


class BlogModel extends Model
{
    /**
     * Get list of posts
     *
     * @param string $category_alias
     * @return array
     */
    public function getPosts(string $category_alias): array {
        $sql = 'SELECT `title`, DATE_FORMAT(`date`, "%M %e, %Y") `date`, `text`, p.`alias`
                FROM post p
                JOIN category c ON p.category_id = c.id
                WHERE c.`alias` = :alias';

        return $this->queryRows($sql, ['alias' => $category_alias]);
    }

    /**
     * Get single post
     *
     * @param string $alias
     * @return array
     */
    public function getPost(string $alias): array {
        $sql = 'SELECT p.`id`, p.`title`, p.`text`, u.`name` author
                FROM post p
                JOIN user u ON p.user_id = u.id
                WHERE `alias` = :alias';

        return $this->queryRow($sql, ['alias' => $alias]);
    }
}