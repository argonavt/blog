<?php
/**
 * Created by argonavt.
 * Date: 12/07/17
 * Time: 8:32 PM
 */

namespace Modules\Blog\Controllers;
use \System\Controller;
use \System\App;
use \Modules\Blog\Models\BlogModel;


class BlogController extends Controller
{
    /**
     * Main page
     */
    public function actionIndex(){
        $data = [];
        App::getInstance()->setParam('title', 'Blog');

        $model = new BlogModel();
        $data['posts'] = $model->getPosts('main');

        $this->render('blog/index', $data);
    }
}