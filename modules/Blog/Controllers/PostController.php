<?php
/**
 * Created by argonavt.
 * Date: 13/07/17
 * Time: 10:37 AM
 */

namespace Modules\Blog\Controllers;
use \System\Controller;
use \System\App;
use \Modules\Blog\Models\BlogModel;
use \Modules\Comments\Controllers\CommentsController;


class PostController extends Controller
{

    /**
     * Post page
     *
     * @param string $alias
     */
    public function actionPost(string $alias){
        $model = new BlogModel();

        $data = $model->getPost($alias);
        App::getInstance()->setParam('title', $data['title']);

        //comments
        $comments = App::getInstance()->renderController(
            new CommentsController(),
            'actionPostComments',
            [$data['id']]
        );
        $data = array_merge($data, ['comments' => $comments]);

        $this->render('blog/post', $data);
    }
}