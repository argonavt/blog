<?php
/**
 * Created by argonavt.
 * Date: 13/07/17
 * Time: 10:56 AM
 */

namespace Modules\Comments\Models;
use \System\Model;


class CommentsModel extends Model
{
    /**
     * Get comments list
     *
     * @param int $post_id
     * @return array
     */
    public function getComments(int $post_id): array {
        $sql = 'SELECT c.`id`, u.`name`, DATE_FORMAT(c.`date`, "%l:%i%p, %M %e, %Y") `date`, c.`text`
                FROM `comment` c
                JOIN `post` p ON p.id = c.post_id
                JOIN `user` u ON c.user_id = u.id
                WHERE p.id = :id';

        return $this->queryRows($sql, ['id' => $post_id]);
    }

    /**
     * Add new comment
     *
     * @param array $data
     */
    public function add(array $data){

        $user_id = $this->getUserId($data['name']);

        //TODO: replace to registration
        if(!$user_id){
            $user_id = $this->addNewUser($data['name']);
        }

        $row = [
            'user_id' => $user_id,
            'text'  => htmlspecialchars($data['text']),
            'post_id' => (int)$data['post_id']
        ];

        $this->insertRow('comment', $row);
    }

    /**
     * Get user ID
     *
     * @param string $name
     * @return bool|int
     */
    private function getUserId(string $name){
        $sql = 'SELECT `id` FROM `user` WHERE name = :name';

        $user = $this->queryRow($sql, ['name' => $name]);

        if(isset($user['id'])){
            return (int) $user['id'];
        }

        return false;
    }

    /**
     * Add new user
     *
     * TODO: replace to registration
     *
     * @param string $name
     * @return int
     */
    private function addNewUser(string $name): int {
        $row = [
            'name' => htmlspecialchars($name),
            'mail'  => "$name@gmail.com",
        ];

        $this->insertRow('user', $row);

        return (int)$this->_pdo->lastInsertId();
    }
}