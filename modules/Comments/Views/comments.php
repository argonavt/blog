<ul>
    <?php foreach($comments as $comment): ?>
        <li>
            <div>User: <?php echo $comment['name'];?></div>
            <div>Date: <?php echo $comment['date'];?></div>
            <div><?php echo $comment['text'];?></div>
        </li>
    <?php endforeach; ?>
</ul>

<h3>Leave your message</h3>
<form action="/comments/add" method="POST">
    <input type="hidden" name="post_id" value="<?php echo $post_id;?>">
    <div>Name: <input type="text" name="name" required></div>
    <div>Message: <textarea name="text" required></textarea></div>
    <div><input type="submit"></div>
</form>