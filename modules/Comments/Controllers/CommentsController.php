<?php
/**
 * Created by argonavt.
 * Date: 12/07/17
 * Time: 5:49 PM
 */

namespace Modules\Comments\Controllers;
use \System\Controller;
use \Modules\Comments\Models\CommentsModel;
use \System\App;


class CommentsController extends Controller
{
    /**
     * @var CommentsModel
     */
    private $__comment_model;

    public function __construct(){
        parent::__construct();
        $this->__comment_model = new CommentsModel();
    }

    /**
     * Show comments
     *
     * @param int $post
     * @return null|string
     */
    public function actionPostComments(int $post){
        $data = [];
        $data['comments'] = $this->__comment_model->getComments($post);
        $data['post_id'] = $post;

        return $this->renderPartial('comments', $data, true);
    }

    /**
     * Add a new comment
     */
    public function actionAdd(){
        $this->__comment_model->add($_POST);
        App::getInstance()->redirect($_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER']: '/');
    }
}