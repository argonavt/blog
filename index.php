<?php

// Require composer autoloader
require __DIR__ . '/vendor/autoload.php';

if (System\Config::get('global\debug')){
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
}

// Create Router instance
$router = new System\Router();

$router->register(System\Config::get('routes'));

// Run it!
$router->run();