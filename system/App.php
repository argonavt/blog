<?php
/**
 * Created by argonavt.
 * Date: 12/07/17
 * Time: 1:18 PM
 */

namespace System;


trait Singleton {
    static private $instance = null;

    private function __construct() { /* ... @return Singleton */ }
    private function __clone() { /* ... @return Singleton */ }

    static public function getInstance() {
        return
            self::$instance===null
                ? self::$instance = new static()//new self()
                : self::$instance;
    }
}


class App
{
    use Singleton;

    /**
     * Custom params
     *
     * @var array $__params
     */
    private $__params = [];

    /**
     * Set custom var
     *
     * @param $key
     * @param $value
     */
    public function setParam($key, $value){
        $this->__params[$key] = $value;
    }

    /**
     * Get custom var
     *
     * @param $key
     * @return $this->__params[$key]
     */
    public function getParam($key){
        return $this->__params[$key];
    }

    /**
     * Get param keys
     *
     * @return array
     */
    public function getParamList(){
        return array_keys($this->__params);
    }

    /**
     * Redirect to another url
     *
     * @param string $url
     * @param int $code
     */
    public function redirect(string $url, $code=302){
        header('Location: '.$url, True, $code);
        die;
    }

    /**
     * Show error message
     *
     * @param string $msg
     */
    public function showError(string $msg){
        echo $msg . "<br><br>\n\n";

        if(function_exists('xdebug_print_function_stack')){
            xdebug_print_function_stack();
            die;
        }

        $e = new \Exception();
        print_r($e->getTraceAsString());
        die;
    }

    /**
     * Get custom rendered page
     *
     * @param Controller $controller
     * @param string $method
     * @param array $params
     * @return string
     */
    public function renderController(Controller $controller, string $method, array $params = []): string {
        return call_user_func_array(array($controller, $method), $params);
    }
}