<?php
/**
 * Created by argonavt.
 * Date: 11/07/17
 * Time: 8:53 PM
 */

namespace System;


class Config
{
    /**
     * Config data
     *
     * @return array
     */
    private static function register(): array {
        return [
            'global' => [
                'debug' => True,
                'site_template' => 'frontend',
            ],
            'routes' => [
                'get' => [
                    '/' => '\\Modules\\Blog\\Controllers\\BlogController@actionIndex',
                    '/post/([a-z0-9-]+)' => '\\Modules\\Blog\\Controllers\\PostController@actionPost',
                ],
                'post' => [
                    '/comments/add' => '\\Modules\\Comments\\Controllers\\CommentsController@actionAdd'
                ]
            ],
            'db' => [
                'host' => getenv('DB_HOST'),
                'name' => getenv('DB_NAME'),
                'user' => getenv('DB_USER'),
                'pass' => getenv('DB_PASS')
            ]
        ];
    }

    /**
     * Return config value
     *
     * @param string $path
     * @return array|mixed
     */
    public static function get(string $path) {
        $data = self::register();

        foreach (explode('\\', $path) as $key){
            $data = $data[$key];
        }

        return $data;
    }

}