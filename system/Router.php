<?php
/**
 * Created by argonavt.
 * Date: 11/07/17
 * Time: 8:19 PM
 */
namespace System;


class Router extends \Bramus\Router\Router
{
    /**
     * Register route rules
     *
     * @param array $routes
     */
    public function register(array $routes){
        foreach ($routes as $method => $rules) {
            foreach ($rules as $path => $action) {
                $this->$method($path, $action);
            }
        }
    }

}