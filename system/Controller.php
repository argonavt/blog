<?php
/**
 * Created by argonavt.
 * Date: 12/07/17
 * Time: 1:03 PM
 */

namespace System;



abstract class Controller
{

    /**
     * Keeps a name of template
     *
     * @var string $template
     */
    protected $template;

    /**
     * Controller constructor.
     *
     * Set controller default data
     */
    public function __construct(){
        $class = new \ReflectionClass($this);
        $this->viewDir = realpath(dirname($class->getFileName()).'/../Views');
        $this->template = Config::get('global\site_template');
    }

    /**
     * Render view with template
     *
     * @param string $view
     * @param array $data
     */
    protected function render(string $view, array $data){

        // set template variables
        foreach (App::getInstance()->getParamList() as $param){
            $$param = App::getInstance()->getParam($param);
        }

        $content = $this->renderPartial($view, $data, True);
        require $_SERVER['DOCUMENT_ROOT'].'/templates/'.$this->template.'.php';
    }

    /**
     * Render view
     *
     * @param string $view
     * @param array $data
     * @param bool $return
     * @return string | null
     */
    protected function renderPartial(string $view, array $data, $return = False){
        if(file_exists($this->viewDir.'/'.$view.'.php')){
            extract($data);
            ob_start();
            require $this->viewDir.'/'.$view.'.php';
            $__content =  ob_get_clean();
        }else{
            if (Config::get('global\debug')){
                App::getInstance()->showError('File '.$this->viewDir.'/'.$view.'.php was not found');
            }else{
                App::getInstance()->redirect('/404');
            }
        }

        if($return){
            return $__content;
        }

        echo $__content;
    }
}