<?php
/**
 * Created by argonavt.
 * Date: 13/07/17
 * Time: 9:31 AM
 */

namespace System;


abstract class Model
{
    /**
     *  PDO object
     */
    protected $_pdo;

    /**
     * Model constructor.
     *
     * set PDO
     */
    function __construct(){
        $dsn = 'mysql:host='.Config::get('db\host').';dbname='.Config::get('db\name').';charset=utf8';

        $opt = array(
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::MYSQL_ATTR_INIT_COMMAND =>"SET time_zone = 'America/Toronto'",
        );

        $this->_pdo = new \PDO($dsn, Config::get('db\user'), Config::get('db\pass'), $opt);
    }

    /**
     * Do PDO query and return one row
     *
     * @param string $sql
     * @param array $data
     * @return array $result
     */
    protected function queryRow(string $sql, array $data = []) {
        $stmt = $this->_pdo->prepare($sql);
        $stmt->execute($data);
        $result = $stmt->fetch();

        if(!$result){
            return [];
        }

        return $result;
    }

    /**
     * Do PDO query and return rows
     *
     * @param string $sql
     * @param array $data
     * @return array
     */
    protected function queryRows(string $sql, array $data = []): array {
        $stmt = $this->_pdo->prepare($sql);

        $stmt->execute($data);

        $data = [];
        foreach ($stmt as $row){
            $data[] =  $row;
        }
        return $data;
    }

    /**
     * Simple insert to db.
     *
     * Preparing and executing query data.
     * Array must be with right keys
     *
     * @param string $table
     * @param array $data
     */
    protected function insertRow(string $table, array $data){
        $columns = '';
        $values = '';

        foreach ($data as $key => $value){
            $columns .= $key.', ';
            $values .= '?, ';
        }

        $sql = "INSERT INTO $table (" .trim($columns, ', '). ")VALUES(" .trim($values, ', '). ")";
        $stmt = $this->_pdo->prepare($sql);
        $stmt->execute(array_values($data));
    }
}